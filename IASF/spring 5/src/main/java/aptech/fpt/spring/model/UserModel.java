package aptech.fpt.spring.model;

import aptech.fpt.spring.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserModel extends PagingAndSortingRepository<User, Integer> {
    Page<User> findAll(Pageable pageable);
}
