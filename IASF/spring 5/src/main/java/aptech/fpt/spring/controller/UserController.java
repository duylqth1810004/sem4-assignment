package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.Product;
import aptech.fpt.spring.entity.ProductValidator;
import aptech.fpt.spring.entity.User;
import aptech.fpt.spring.model.ProductModel;
import aptech.fpt.spring.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserModel userModel;

    @RequestMapping(path = "/user/create", method = RequestMethod.GET)
    public String createUser(@ModelAttribute User u) {
        return "user-form";
    }

    @RequestMapping(path = "/user/save", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute User user, Model model) {
        model.addAttribute("user", user);
        userModel.save(user);
        return "redirect:/user/list";
    }

    @RequestMapping(path = "/user/edit/{id}", method = RequestMethod.GET)
    public String editUser(@PathVariable int id, Model model) {
        Optional<User> optionalUser = userModel.findById(id);
        if (optionalUser.isPresent()) {
            model.addAttribute("user", optionalUser.get());
            return "user-form";
        } else {
            return "not-found";
        }
    }

//    @RequestMapping(path = "/product/edit/{id}", method = RequestMethod.POST)
//    public String updateProduct(@PathVariable int id,@Valid Product product, BindingResult result, Model model){
//        Optional<Product> optionalProduct = productModel.findById(id);
//        if (optionalProduct.isPresent()) {
//            if (result.hasErrors()) {
//                return "product-form";
//            }
//            productModel.save(product);
//            return "redirect:/product/list";
//        } else {
//            return "not-found";
//        }
//    }

    @RequestMapping(path = "/user/delete/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable int id) {
        Optional<User> optionalUser = userModel.findById(id);
        if (optionalUser.isPresent()) {
            userModel.delete(optionalUser.get());
            return "redirect:/user/list";
        } else {
            return "not-found";
        }
    }

    @RequestMapping(path = "/user/list", method = RequestMethod.GET)
    public String getListProduct(Model model, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<User> pagination = userModel.findAll(PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        model.addAttribute("datetime", Calendar.getInstance().getTime());
        return "user-list";
    }
}
