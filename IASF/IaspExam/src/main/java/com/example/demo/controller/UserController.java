package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserRepository um;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index(Model model){
        List<User> users = (List<User>) um.findAll();

        model.addAttribute("users", users);

        return "index";
    }

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String save(User user){
        um.save(user);
        return "redirect:/";
    }

    @RequestMapping(path = "/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam("id") int userId, Model model){
        Optional<User> userEdit = um.findById(userId);
        userEdit.ifPresent(user -> model.addAttribute("user", user));
        return "redirect:/";
    }

    @RequestMapping(path = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id") int userId, Model model){
        um.deleteById(userId);
        return "redirect:/";
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public String searchProductByName(@RequestParam("search") String name, Model model) {
        if (model == null){
            return "redirect:/";
        }

        List<User> users = um.findByName(name);
        model.addAttribute("users", users);

        return "index";
    }
}
