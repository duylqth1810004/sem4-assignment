package com.example.sqllitepart2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "USER";
    public static final Integer DB_VERSION = 1;

    public static String TABLE_NAME = "TBL_USER";
    public static String ID = "_id";
    public static String NAME = "name";
    public static String GENDER = "gender";
    public static String DES = "des";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + " ( " +
                ID + " INTEGER PRIMARY KEY, " +
                NAME + " TEXT, " +
                GENDER + " TEXT, " +
                DES + " TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    public String addUser(String user, String gender, String des) {
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(NAME, user);
        cv.put(GENDER, gender);
        cv.put(DES, des);

        long isAdded = db.insert(TABLE_NAME, null, cv);

        if (isAdded == -1) {
            return "Add Fail";
        }
        db.close();

        return "Add Success";
    }

    public String updateUser(int id, String user, String gender, String des) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(NAME, user);
        cv.put(GENDER, gender);
        cv.put(DES, des);

        int isUpdated = db.update(TABLE_NAME, cv, ID + " ? ", new String[] {id + ""});

        if (isUpdated > 0) {
            return "Update Success";
        }
        db.close();
        return "Update Failed!";
    }

    public String deleteUser(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int isDeleted = db.delete(TABLE_NAME, ID + " ? ", new String[]{id + ""});

        if (isDeleted > 0) {
            return "Delete Success!";
        }

        db.close();

        return "Delete Failed!";
    }

    public Cursor getAllUser() {
        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT * FROM " + TABLE_NAME;

        Cursor c = db.rawQuery(sql, null);

        return c;
    }
}
