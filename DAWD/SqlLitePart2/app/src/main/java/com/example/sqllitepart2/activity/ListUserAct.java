package com.example.sqllitepart2.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sqllitepart2.R;
import com.example.sqllitepart2.database.DbHelper;

public class ListUserAct extends AppCompatActivity {
    private DbHelper db;
    private Cursor c;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);

        db = new DbHelper(this);
        ListView lvUser = (ListView) findViewById(R.id.lvUser);

        c = db.getAllUser();

        adapter = new SimpleCursorAdapter(this, R.layout.item_user, c, new String[] {DbHelper.ID, DbHelper.NAME, DbHelper.GENDER}, new int[]{
                R.id.tvId, R.id.tvName, R.id.tvGender}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        lvUser.setAdapter(adapter);
        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) adapter.getItem(position);
                int _id = cursor.getInt(cursor.getColumnIndex(DbHelper.ID));
                String name = cursor.getString(cursor.getColumnIndex(DbHelper.NAME));
                String gender = cursor.getString(cursor.getColumnIndex(DbHelper.GENDER));
                String des = cursor.getString(cursor.getColumnIndex(DbHelper.DES));

                Intent intent = new Intent(ListUserAct.this, UpdateAct.class);
                intent.putExtra(DbHelper.ID, _id);
                intent.putExtra(DbHelper.NAME, name);
                intent.putExtra(DbHelper.GENDER, gender);
                intent.putExtra(DbHelper.DES, des);

                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        c = db.getAllUser();

        adapter.changeCursor(c);
        adapter.notifyDataSetChanged();
        db.close();
    }
}
