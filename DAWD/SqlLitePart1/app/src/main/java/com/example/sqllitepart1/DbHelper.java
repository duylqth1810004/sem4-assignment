package com.example.sqllitepart1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "User.db";
    public static final String TABLE_NAME = "Tbl_User";
    public static final Integer DB_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + " ( " +
                Tbl_User.id + " INTEGER PRIMARY KEY, " +
                Tbl_User.name + " TEXT, " +
                Tbl_User.age + " INTEGER, " +
                Tbl_User.gender + " TEXT )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {}

    public String insertDb(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(Tbl_User.name, user.getName());
        cv.put(Tbl_User.age, user.getAge());
        cv.put(Tbl_User.gender, user.getGender());

        long isSuccess = db.insert(TABLE_NAME, null, cv);

        if (isSuccess > 0) {
            return "Success!";
        } else {
            return "Failed!";
        }
    }

    public List<User> getAllUser() {
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "SELECT * FROM " + TABLE_NAME;

        List<User> userList = new ArrayList<>();

        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                User user = new User();
                user.setName(cursor.getString(cursor.getColumnIndex(Tbl_User.name)));
                user.setAge(cursor.getInt(cursor.getColumnIndex(Tbl_User.age)));
                user.setGender(cursor.getString(cursor.getColumnIndex(Tbl_User.gender)));
                user.setId(cursor.getInt(cursor.getColumnIndex(Tbl_User.id)));
            } while (cursor.moveToNext());
        }

        return userList;
    }

    public String updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(Tbl_User.name, user.getName());
        cv.put(Tbl_User.age, user.getAge());

        long isSuccess = db.update(TABLE_NAME, cv, "id=" + user.getId(), null);

        if (isSuccess > 0) {
            return "Update Success!";
        } else {
            return "Update Failed!";
        }
    }

    public String deleteUser(int userId) {
        SQLiteDatabase db = this.getWritableDatabase();

        long isSuccess = db.delete(TABLE_NAME, "id=" + userId, null);

        if (isSuccess > 0) {
            return "Delete Success!";
        } else {
            return "Delete Failed!";
        }
    }
}
