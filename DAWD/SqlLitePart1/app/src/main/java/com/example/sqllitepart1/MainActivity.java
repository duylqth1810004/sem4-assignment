package com.example.sqllitepart1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private DbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DbHelper(this);

        insertUser();

        getAllUser();

        updateUser();

        deleteUser();
    }

    private void insertUser() {
        User user = new User("Dendi", "Male", 30);
        String message = db.insertDb(user);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void getAllUser() {
        List<User> userList = db.getAllUser();
        for (User u : userList) {
            Log.d("USER: " , "Name: " + u.getName() + " - Id: " + u.getId() + " - Age: " + u.getAge() + " - Gender: " + u.getGender());
        }
    }

    private void updateUser() {
        User u = new User();
        u.setId(1);
        u.setName("Dendi Updated!");
        u.setAge(27);
        u.setGender("Still Male bro!");

        String message = db.updateUser(u);

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void deleteUser() {
        db.deleteUser(1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        db.close();
    }
}