package com.example.listviewadvanced;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IOnChildItemClick{

    private List<ContactModel> listContact = new ArrayList<>();
    private ListView lvContact;
    private ContactAdapter mAdapter;
    private ImageView ivUser;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();
        initView();

        mAdapter = new ContactAdapter(this, listContact);
        mAdapter.registerChildItemClick(this);
        lvContact.setAdapter(mAdapter);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel model = listContact.get(position);
                Toast.makeText(MainActivity.this, model.getName() + ": " + model.getPhone(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView () {
        lvContact = (ListView) findViewById(R.id.lvContact);
        ivUser = (ImageView) findViewById(R.id.ivUser);
        tvName = (TextView) findViewById(R.id.tvName);
    }

    private void initData () {
        listContact.add(new ContactModel("Andree", "01234567890", R.drawable.user_a));
        listContact.add(new ContactModel("Right", "0944939213", R.drawable.user_b));
        listContact.add(new ContactModel("Hand", "0904710530", R.drawable.user_c));
        listContact.add(new ContactModel("Dash", "01276860863", R.drawable.user_d));
        listContact.add(new ContactModel("Sunny", "0816860670", R.drawable.user_a));
        listContact.add(new ContactModel("Kido", "01234567890", R.drawable.user_b));
        listContact.add(new ContactModel("Viet", "01234567890", R.drawable.user_c));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
        listContact.add(new ContactModel("Dragon", "01234567890", R.drawable.user_d));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.unRegisterChildItemClick();
    }

    @Override
    public void onItemChildClick(int position) {
        ContactModel contact = listContact.get(position);
        ivUser.setImageResource(contact.getImage());
        tvName.setText(contact.getName());
    }
}