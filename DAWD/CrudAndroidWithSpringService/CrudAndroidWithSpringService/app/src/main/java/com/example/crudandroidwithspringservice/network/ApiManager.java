package com.example.crudandroidwithspringservice.network;

import com.example.crudandroidwithspringservice.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiManager {
    String SERVER_URL = "https://fullstack-api-retrofit.herokuapp.com/api/v1/user";

    @GET
    Call<List<User>> getUserList();

    @Headers({"Content-Type: application/json"})
    @PUT("/{id}")
    Call<User> updateUser(@Path("id") int id, @Body User user);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<User> addUser(@Body User user);

    @DELETE("/{id}")
    Call<String> deleteUser(@Path("id") int id);
}
