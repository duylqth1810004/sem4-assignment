package com.example.crudandroidwithspringservice;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.crudandroidwithspringservice.model.User;
import com.example.crudandroidwithspringservice.network.ApiManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateUserActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText etName;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etEmail;
    private Button btSubmit;
    private User user;
    private User newUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        btSubmit = (Button) findViewById(R.id.btSubmit);
        btSubmit.setOnClickListener(this);
        initView();

        user = (User) getIntent().getSerializableExtra("user");
        if(user != null){
            etName.setText(user.getName().toString());
            etEmail.setText(user.getEmail().toString());
            etPassword.setText(user.getPassword().toString());
            etUsername.setText(user.getUsername().toString());
        }
    }

    private void initView() {
        etName = (EditText) findViewById(R.id.edName);
        etUsername = (EditText) findViewById(R.id.edUsername);
        etPassword = (EditText) findViewById(R.id.edPassword);
        etEmail =  (EditText) findViewById(R.id.edEmail);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btSubmit:
                onSubmit();
                break;
            default:
                break;
        }

    }

    private void onSubmit() {
        newUser = new User() ;
        newUser.setName(etName.getText().toString());
        newUser.setEmail(etEmail.getText().toString());
        newUser.setPassword(etPassword.getText().toString());
        newUser.setUsername(etUsername.getText().toString());
        initView();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiManager.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiManager service = retrofit.create(ApiManager.class);
        if(user != null ){

            service.updateUser(user.getId() , newUser).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    Toast.makeText(CreateUserActivity.this, "Update Success" , Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateUserActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(CreateUserActivity.this, "Update Fail" , Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            service.addUser(newUser).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    Toast.makeText(CreateUserActivity.this, "Create Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateUserActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(CreateUserActivity.this, "Create Fail", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

