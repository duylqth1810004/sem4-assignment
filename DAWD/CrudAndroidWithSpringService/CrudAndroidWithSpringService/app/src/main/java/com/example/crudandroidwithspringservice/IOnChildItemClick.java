package com.example.crudandroidwithspringservice;

    public interface IOnChildItemClick {
        void onItemChildClickEdit(int position);
        void onItemChildClickDelete(int position);
        void onItemChildClickDetail(int position);
    }

