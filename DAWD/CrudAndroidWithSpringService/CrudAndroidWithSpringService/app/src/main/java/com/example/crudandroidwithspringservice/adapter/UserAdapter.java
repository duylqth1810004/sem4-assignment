package com.example.crudandroidwithspringservice.adapter;

import android.app.Activity;
import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.crudandroidwithspringservice.IOnChildItemClick;
import com.example.crudandroidwithspringservice.R;
import com.example.crudandroidwithspringservice.model.User;

import java.util.List;

public class UserAdapter extends BaseAdapter {
    private List<User> userList;
    private IOnChildItemClick iOnChildItemClick;
    public Context context;

    public UserAdapter(List<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    public void setUsers(List<User> userList) {
        this.userList = userList;
        this.notifyDataSetChanged();
    }

    public void setOnChildItemClick(IOnChildItemClick iOnChildItemClick) {
        this.iOnChildItemClick = iOnChildItemClick;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int i, View convevtView, ViewGroup parent) {
        View rowView = convevtView;

        if (rowView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();

            rowView = inflater.inflate(R.layout.activity_item, null);

            viewUser user = new viewUser();

            user.tvName = (TextView) rowView.findViewById(R.id.tvName);
            user.tvUsername = (TextView) rowView.findViewById(R.id.tvUsername);
            user.tvPassword = (TextView) rowView.findViewById(R.id.tvPassword);
            user.tvEmail = (TextView) rowView.findViewById(R.id.tvEmail);

            user.btDelete = (ImageButton) rowView.findViewById(R.id.btDelete);
            user.btDetail = (ImageButton) rowView.findViewById(R.id.btDetail);
            user.btEdit = (ImageButton) rowView.findViewById(R.id.btEdit);

            rowView.setTag(user);
        }

        viewUser user = (viewUser) rowView.getTag();

        user.tvName.setText(userList.get(i).getName());
        user.tvUsername.setText(userList.get(i).getUsername());

        user.btDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnChildItemClick.onItemChildClickDetail(i);
            }
        });

        user.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnChildItemClick.onItemChildClickDelete(i);
            }
        });

        user.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnChildItemClick.onItemChildClickEdit(i);
            }
        });

        return rowView;
    }

    static class viewUser {
        ImageButton btEdit;
        ImageButton btDetail;
        ImageButton btDelete;

        TextView tvName;
        TextView tvUsername;
        TextView tvPassword;
        TextView tvEmail;
    }
}
