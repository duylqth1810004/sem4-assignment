package com.example.listviewbasic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvContact;
    private List<ContactModel> listContact = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initData();

        lvContact = (ListView) findViewById(R.id.lvContact);

        ContactAdapter adapter = new ContactAdapter(listContact, this);

        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = listContact.get(position);
                Toast.makeText(MainActivity.this, contactModel.getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        ContactModel contact = new ContactModel("User A", "0944939213", R.drawable.user_a);
        listContact.add(contact);
        contact = new ContactModel("User B", "01276860863", R.drawable.user_b);
        listContact.add(contact);
        contact = new ContactModel("User C", "0904710530", R.drawable.user_c);
        listContact.add(contact);
        contact = new ContactModel("User D", "01234567890", R.drawable.user_d);
        listContact.add(contact);
    }
}