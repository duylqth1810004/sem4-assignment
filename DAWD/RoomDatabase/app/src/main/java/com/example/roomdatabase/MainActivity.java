package com.example.roomdatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.roomdatabase.database.AppDatabase;
import com.example.roomdatabase.database.BookmarkEntity;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getAppDatabase(this);

        insertBookmark();

        updateBookmark(1);

        getAllBookmark();

        findBookmark(1);
    }

    private void insertBookmark() {
        BookmarkEntity bm = new BookmarkEntity();

        bm.title = "Con meo xoe 2 cai canh";

        bm.content = "Con vit keu meo meo";

        db.bookmarkDao().insertBookmark(bm);
    }

    private void updateBookmark(int id) {
        BookmarkEntity bm = db.bookmarkDao().getBookmark(id);

        bm.title = "Con meo keu meo meo";

        db.bookmarkDao().updateBookmark(bm);
    }

    private void findBookmark(int id) {
        BookmarkEntity model = db.bookmarkDao().getBookmark(id);

        Log.d("TAG", "Find Bookmark with Id: " + model.id + " - Title: " + model.title);
    }

    private void deleteBookmark(int id) {
        BookmarkEntity model = db.bookmarkDao().getBookmark(id);

        db.bookmarkDao().deleteBookmark(model);
    }

    private void deleteAllBookmark() {
        db.bookmarkDao().deleteAll();
    }

    private void getAllBookmark() {
        List<BookmarkEntity> list = db.bookmarkDao().getAllBookmark();

        for (BookmarkEntity b : list) {
            Log.d("TAG: ", "Id: " + b.id + " - Title: " + b.title);
        }
    }
}