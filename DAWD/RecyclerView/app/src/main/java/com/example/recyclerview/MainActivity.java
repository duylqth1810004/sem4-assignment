package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Product> listProduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Phase 1: Create Data Source
        initData();

        //Phase 2: Create Adapter
        ProductAdapter adapter = new ProductAdapter(this, listProduct);

        //Phase 3: Create Layout Manager
        //Grid Layout
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        //Linear Layout
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);

        //Phase 4: Create and Deploy RV
        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData() {
        listProduct.add(new Product("Product 1", "500", R.drawable.p1));
        listProduct.add(new Product("Product 2", "700", R.drawable.p2));
        listProduct.add(new Product("Product 3", "900", R.drawable.p3));
        listProduct.add(new Product("Product 4", "1100", R.drawable.p4));
        listProduct.add(new Product("Product 5", "1300", R.drawable.p5));
        listProduct.add(new Product("Product 6", "1500", R.drawable.p6));
    }
}