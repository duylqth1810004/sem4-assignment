package com.example.retrofitnetwork.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
