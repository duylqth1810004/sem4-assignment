package com.example.dawdexam.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {
    @Insert(onConflict = REPLACE)
    void insertUser(User u);

    @Query("SELECT COUNT(*) FROM User")
    Integer countAllUser();
}
