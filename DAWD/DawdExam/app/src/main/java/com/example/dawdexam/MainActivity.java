package com.example.dawdexam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.dawdexam.database.AppDatabase;
import com.example.dawdexam.database.User;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AppDatabase db;

    private EditText edName;
    private EditText edEmail;
    private Spinner spinner;
    private EditText edDes;
    private CheckBox ck;
    private Button btSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getAppDatabase(this);

        btSend = (Button) findViewById(R.id.btSend);

        btSend.setOnClickListener(this);

        spinner = (Spinner) findViewById(R.id.spinner);

        String[] genders = {"Male", "Female", "Unknow"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, genders);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btSend:
                insertUser();
                break;
            default:
                break;
        }
    }


    private void insertUser() {
        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.edEmail);
        spinner = (Spinner) findViewById(R.id.spinner);
        edDes = (EditText) findViewById(R.id.edDes);
        ck = (CheckBox) findViewById(R.id.ck);

        String edNameString = edName.getText().toString();
        String edEmailString = edEmail.getText().toString();
        String edSpinnerString = spinner.getSelectedItem().toString();
        String edDesString = edDes.getText().toString();

        if (!edNameString.isEmpty() && !edEmailString.isEmpty() && !edSpinnerString.isEmpty() && !edDesString.isEmpty()) {
            User u = new User();

            u.setName(edNameString);
            u.setEmail(edEmailString);
            u.setGender(edSpinnerString);
            u.setDescription(edDesString);

            try {
                db.userDao().insertUser(u);

                getCountUser();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Insert Error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please fill all field!", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void getCountUser() {
        int count;

        count = db.userDao().countAllUser();

        Toast.makeText(this, "Records quantiti(es) in Database: " + count, Toast.LENGTH_SHORT).show();
    }






}