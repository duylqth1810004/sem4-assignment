package com.example.cswexam.api;

import com.example.cswexam.entity.Product;
import com.example.cswexam.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
@Slf4j
@RequiredArgsConstructor
public class ProductAPI {
    final private ProductService pS;

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        return ResponseEntity.ok(pS.findAll());
    }

    @PostMapping
    public ResponseEntity addProduct(@Valid @RequestBody Product p) {
        return ResponseEntity.ok(pS.save(p));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable Integer id) {
        Optional<Product> pro = pS.findById(id);

        if (!pro.isPresent()) {
            log.error("Id not Existed!");
        }

        return ResponseEntity.ok(pro.get());
    }

    @PutMapping("/{id}&sellQuantity={sellQuantity}")
    public ResponseEntity<Product> update(@PathVariable Integer id, @PathVariable Integer sellQuantity) {
        Optional<Product> pro = pS.findById(id);

        if (!pro.isPresent()) {
            log.error("Id not Existed!");
        }

        if (pro.get().getQuantity() > sellQuantity) {

            //Bắt buộc phải có số lượng còn phải lớn hơn lượng bán đi mới cho trừ
            pro.get().setQuantity(pro.get().getQuantity() - sellQuantity);

            return ResponseEntity.ok(pS.save(pro.get()));
        } else {

            return ResponseEntity.badRequest().build();

        }

    }
}