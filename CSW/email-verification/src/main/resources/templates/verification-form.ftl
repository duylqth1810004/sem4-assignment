<#import "/spring.ftl" as spring />

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Activate account with SpringBoot and REST</title>
    </head>
    <body>
        <h2>Verify your Email</h2>
        <@spring.bind "verificationForm"/>
        <#if verificationForm?? && noErrors??>
            Send a confirmation link to your inbox ${verificationForm.email}<br>
        <#else>
            <form action="/email-verification" method="post">
                Email:<br>
                <@spring.formInput "verificationForm.email"/>
                <@spring.showErrors "<br>"/>
                <br><br>
                <input type="submit" value="Submit">
            </form>
        </#if>
    </body>
</html>