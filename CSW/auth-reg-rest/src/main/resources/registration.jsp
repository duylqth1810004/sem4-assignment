<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Create an Account</title>
        <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <form:form method="post" modelAttribute="userForm" class="form-signin">
                <h2 class="form-signin-heading">Create your Account</h2>
                <spring:bind path="username">
                    <div class="form-group" ${status.error ? 'has-error' : ''}">
                        <form:input path="username" type="text" class="form-control" placeholder="Username" autofocus="true"></form:input>
                        <form:errors path="username"></form:errors>
                    </div>
                </spring:bind>
                <spring:bind path="password">
                    <div class="form-group" ${status.error ? 'has-error' : ''}">
                    <form:input path="password" type="password" class="form-control" placeholder="Password"></form:input>
                    <form:errors path="password"></form:errors>
                    </div>
                </spring:bind>
                <spring:bind path="passwordConfirm">
                    <div class="form-group" ${status.error ? 'has-error' : ''}">
                    <form:input path="passwordConfirm" type="password" class="form-control" placeholder="Confirm your Password"></form:input>
                    <form:errors path="passwordConfirm"></form:errors>
                    </div>
                </spring:bind>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form:form>
        </div>
    </body>
</html>