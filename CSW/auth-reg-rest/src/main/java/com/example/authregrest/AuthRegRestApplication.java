package com.example.authregrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthRegRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthRegRestApplication.class, args);
    }

}
