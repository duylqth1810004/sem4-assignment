package com.example.authregrest.service;

import com.example.authregrest.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService  {
    void save(User user);

    User findByUsername(String username);
}
