<%--
  Created by IntelliJ IDEA.
  User: Duy Lumiere
  Date: 28/08/2020
  Time: 8:22 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Result</title>
        <link rel="stylesheet" href="css/result.css">
    </head>
    <body>
        <div id="container">
            <h1>Search Result</h1>
            <a href="search.jsp">Back to Search Page</a>
            <jsp:useBean id="finder" class="com.wpsj.model.ProductFinderBean" scope="request"/>
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${finder.products}" var="product">
                        <tr>
                            <td><c:out value="${product.id}"/></td>
                            <td><c:out value="${product.name}"/></td>
                            <td><c:out value="${product.description}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
