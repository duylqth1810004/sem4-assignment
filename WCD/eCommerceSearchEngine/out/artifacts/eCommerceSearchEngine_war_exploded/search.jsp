<%--
  Created by IntelliJ IDEA.
  User: Duy Lumiere
  Date: 28/08/2020
  Time: 8:22 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Product</title>
        <link rel="stylesheet" href="css/search.css">
    </head>
    <body>
        <div id="container">
            <h1>Search Product</h1>
            <form action="ProductFinder">
                <span style="color: red">
                    <c:out value="${param.msg}"/>
                </span>
                <input name="name" id="name-inp">
                <br>
                <input type="submit" id="submit-inp">
            </form>
        </div>
    </body>
</html>
