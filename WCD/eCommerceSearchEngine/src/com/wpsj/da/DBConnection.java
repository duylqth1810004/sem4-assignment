package com.wpsj.da;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static Connection connection;

//    Test DB Connect!
//    public static void main(String[] args) {
//        DBConnection dbConnection = new DBConnection();
//        try {
//            dbConnection.getConnection();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//    }

    public Connection getConnection() throws ClassNotFoundException, SQLException
    {
        if (connection == null)
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/t1811e?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            System.out.println("Connect to database Success!");
        }
        return connection;
    }
}
