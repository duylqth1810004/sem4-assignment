package com.wpsj.da;

import com.wpsj.entity.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ProductDataAccess {
    private PreparedStatement searchStatement;

    private PreparedStatement getSearchStatement() throws ClassNotFoundException, SQLException
    {
        if (searchStatement == null)
        {
            Connection connection = new DBConnection().getConnection();
            searchStatement = connection.prepareStatement(
                    "SELECT * FROM product WHERE name like ?"
            );
        }
        return searchStatement;
    }

    public List<Product> getProductsByName(String name)
    {
        try
        {
            PreparedStatement pstm = getSearchStatement();

            pstm.setString(1, "%" + name + "%");

            ResultSet rs = pstm.executeQuery();

            List<Product> products = new LinkedList<Product>();

            while (rs.next()){
                products.add(new Product(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description")));
            }

            return products;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
