package com.example.controller;

import com.example.da.UserDAO;
import com.example.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/")
public class UserServlet extends HttpServlet {
    private UserDAO userDAO;

    public void init() {userDAO = new UserDAO();}

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action = req.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(req, res);
                    break;
                case "/insert":
                    insertUser(req, res);
                    break;
                case "/delete":
                    deleteUser(req, res);
                    break;
                case "/edit":
                    showEditForm(req, res);
                    break;
                case "/update":
                    updateUser(req, res);
                    break;
                default:
                    listUser(req, res);
                    break;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
    }

    private void listUser(HttpServletRequest req, HttpServletResponse res) throws SQLException, IOException, ServletException, ClassNotFoundException {
        List<User> listUser = userDAO.selectAllUsers();

        req.setAttribute("listUser", listUser);

        RequestDispatcher dispatcher = req.getRequestDispatcher("user-list.jsp");
        dispatcher.forward(req, res);
    }

    private void showNewForm(HttpServletRequest req, HttpServletResponse res) throws SQLException, ServletException, IOException, ClassNotFoundException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("user-form.jsp");
        dispatcher.forward(req, res);
    }

    private void showEditForm(HttpServletRequest req, HttpServletResponse res) throws SQLException, ServletException, IOException, ClassNotFoundException {
        int id = Integer.parseInt(req.getParameter("id"));

        User existedUser = userDAO.selectUser(id);

        RequestDispatcher dispatcher = req.getRequestDispatcher("user-form.jsp");
        req.setAttribute("user", existedUser);
        dispatcher.forward(req, res);
    }

    private void insertUser(HttpServletRequest req, HttpServletResponse res) throws ServletException, SQLException, IOException, ClassNotFoundException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String country = req.getParameter("country");

        User insUser = new User(name, email, country);

        userDAO.insertUser(insUser);

        res.sendRedirect("list");
    }

    private void updateUser(HttpServletRequest req, HttpServletResponse res) throws SQLException, IOException, ClassNotFoundException {
        int id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String country = req.getParameter("country");

        User updUser = new User(id, name, email, country);

        userDAO.updateUser(updUser);

        res.sendRedirect("list");
    }

    private void deleteUser(HttpServletRequest req, HttpServletResponse res) throws SQLException, ServletException, IOException, ClassNotFoundException {
        int id = Integer.parseInt(req.getParameter("id"));

        userDAO.deleteUser(id);

        res.sendRedirect("list");
    }
}
