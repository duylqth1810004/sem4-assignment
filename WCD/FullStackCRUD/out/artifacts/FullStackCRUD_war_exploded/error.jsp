<%--
  Created by IntelliJ IDEA.
  User: Duy Lumiere
  Date: 10/09/2020
  Time: 1:59 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html>

<html>
<head>
    <title>Error Page</title>
</head>
<body>
    <h1>Error</h1>
    <h2><%=exception.getMessage()%><br></h2>
</body>
</html>