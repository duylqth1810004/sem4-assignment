<%--
  Created by IntelliJ IDEA.
  User: Duy Lumiere
  Date: 10/09/2020
  Time: 1:42 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Employee Management App</title>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="...">
        <div>
            <a href=""> Add Employee </a>
        </div>
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/list" class="nav-link">Employee</a></li>
        </ul>
    </nav>
</header>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <form action="insert" method="post">
                    <caption>
                        <h2>
                                Add New User
                        </h2>
                    </caption>
                    <fieldset class="form-group">
                        <label>Full Name</label>
                        <input type="text" placeholder="Enter Fullname here" class="form-control" name="fullname" required>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Birthday</label>
                        <input type="text" placeholder="Enter Birthday here" class="form-control" name="birthday" required>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Address</label>
                        <input type="text" placeholder="Enter Address here" class="form-control" name="address" required>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Position</label>
                        <input type="text" placeholder="Enter Position here" class="form-control" name="position" required>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Department</label>
                        <input type="text" placeholder="Enter Department here" class="form-control" name="department" required>
                    </fieldset>
                    <button type="submit" class="btn btn-success">Submit</button>
                <br>
                <button type="button" class="btn btn-warning" onclick="resetForm()">Reset</button>
                </form>
        </div>
    </div>
</div>
</body>
<script>
    function resetForm(){
        $('.form-control').val('');
    }
</script>
</html>