<%--
  Created by IntelliJ IDEA.
  User: Duy Lumiere
  Date: 10/09/2020
  Time: 1:23 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Employee Management App</title>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="...">
        <div>
            <a href="" class="navbar-brand">
                Employee Management App </a>
        </div>
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/list" class="nav-link">Employee</a></li>
        </ul>
    </nav>
</header>
<br>
<div class="row">
    <div class="container">
        <h3 class="text-center">List of Employees</h3>
        <hr>
        <div class="container text-left">
            <a href="<%=request.getContextPath()%>/new" class="btn btn-success">Add New User
            </a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Id</th>
                <th>Fullname</th>
                <th>Birthday</th>
                <th>Address</th>
                <th>Position</th>
                <th>Department</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="emp" items="${listEmployee}">
                <tr>
                    <td>
                        <c:out value="${emp.id}"/>
                    </td>
                    <td>
                        <c:out value="${emp.fullname}"/>
                    </td>
                    <td>
                        <c:out value="${emp.birthday}"/>
                    </td>
                    <td>
                        <c:out value="${emp.address}"/>
                    </td>
                    <td>
                        <c:out value="${emp.position}"/>
                    </td>
                    <td>
                        <c:out value="${emp.department}"/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
