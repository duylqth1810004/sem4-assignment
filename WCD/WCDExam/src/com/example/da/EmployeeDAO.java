package com.example.da;

import com.example.entity.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {
    private String jdbcURL = "jdbc:mysql://127.0.0.1:3306/wcdexam?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private String jdbcUsername = "root";
    private String jdbcPassword = "";

    private static final String INSERT_EMP_SQL = "INSERT INTO employee" + " (fullname, birthday, address, position, department) VALUES" + " (?, ?, ?, ?, ?);";
    private static final String SELECT_ALL_EMPS = "SELECT * FROM employee;";

    public EmployeeDAO() {}

    protected Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);

        return  connection;
    }

    public void insertEmployee(Employee emp) throws SQLException, ClassNotFoundException {
        Connection connection = getConnection();

        PreparedStatement ps = connection.prepareStatement(INSERT_EMP_SQL);
        ps.setString(1, emp.getFullname());
        ps.setString(2, emp.getBirthday());
        ps.setString(3, emp.getAddress());
        ps.setString(4, emp.getPosition());
        ps.setString(5, emp.getDepartment());
        ps.executeUpdate();
    }

    public List<Employee> selectAllEmployees() throws SQLException, ClassNotFoundException {
        List<Employee> listEmployee = new ArrayList<>();
        Connection connection = getConnection();
        PreparedStatement ps = connection.prepareStatement(SELECT_ALL_EMPS);
        ResultSet rs = ps.executeQuery();

        while (rs.next()){
            int id = rs.getInt("id");
            String fullname = rs.getString("fullname");
            String birthday = rs.getString("birthday");
            String address = rs.getString("address");
            String position = rs.getString("position");
            String department = rs.getString("department");
            listEmployee.add(new Employee(id, fullname, birthday, address, position, department));
        }

        return listEmployee;
    }
}
