package com.example.controller;

import com.example.da.EmployeeDAO;
import com.example.entity.Employee;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@WebServlet("/")
public class EmployeeServlet extends HttpServlet {
    private EmployeeDAO employeeDAO;

    public void init() {employeeDAO = new EmployeeDAO();}

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action = req.getServletPath();

        try {
            switch (action) {
                case "/list":
                    listEmployee(req, res);
                    break;
                case "/insert":
                    insertEmployee(req, res);
                    break;
                default:
                    showNewForm(req, res);
                    break;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
    }

    private void listEmployee(HttpServletRequest req, HttpServletResponse res) throws SQLException, IOException, ServletException, ClassNotFoundException {
        List<Employee> listEmployee = employeeDAO.selectAllEmployees();

        req.setAttribute("listEmployee", listEmployee);

        RequestDispatcher dispatcher = req.getRequestDispatcher("list.jsp");
        dispatcher.forward(req, res);
    }

    private void showNewForm(HttpServletRequest req, HttpServletResponse res) throws SQLException, ServletException, IOException, ClassNotFoundException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("employee.jsp");
        dispatcher.forward(req, res);
    }

    private void insertEmployee(HttpServletRequest req, HttpServletResponse res) throws ServletException, SQLException, IOException, ClassNotFoundException {
        String fullname = req.getParameter("fullname");
        String birthday = req.getParameter("birthday");
        String address = req.getParameter("address");
        String position = req.getParameter("position");
        String department = req.getParameter("department");

        Employee insEmp = new Employee(fullname, birthday, address, position, department);

        employeeDAO.insertEmployee(insEmp);

        res.sendRedirect("list.jsp");
    }
}
