/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.HRManager.bol;

import example.ConvertData;
import example.HRManager.dal.DAO;
import example.HRManager.entities.Employee;
import example.ValidData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class EmployeeBO {

    /**
     *
    add new Employee to database
     */
  /*    public static void main(String[] args){
          ConvertData cv = new ConvertData();

          Date date = new Date();

          cv.date2string( , ValidData.EN_DATE);
    }*/
    public int add(Employee e) {
        ConvertData cv = new ConvertData();
        String sql = "INSERT INTO hrmanager (lastName, firstName, birthDate, hireDate, address, city, country, " +
                "homePhone, mobile, email, note ,photoPath) VALUES('" + e.getLastName() + "', '" + e.getFirstName() +
                "', '" +"1995/12/12" +
                "', '" +"1999/12/12" + "', '" +
                e.getAddress() + "', '" + e.getCity() + "', '" + e.getCountry() + "', '" + e.getHomePhone() + "', '" +
                e.getMobile() + "', '" + e.getEmail() + "', '" + e.getNote() + "' , '')";
        DAO dao = new DAO();
        System.out.println(sql);
        return dao.executeUpdateQuery(sql);
    }

    /**
     *
    delete Employee into database
     */
    public int delete(Employee e) {
        String sql = "DELETE FROM hrmanager WHERE EmployeeID=" + e.getEmployeeID();
        DAO dao = new DAO();
        return dao.executeUpdateQuery(sql);
    }

    /**
     *
    update old Employee  by new Employee into database
     */
    public int edit(Employee e) {
        ConvertData cv = new ConvertData();
        String sql = "UPDATE hrmanager SET lastName='" + e.getLastName() + "', firstName='" + e.getFirstName() +
                "', birthDate='" + cv.date2string(e.getBirthDate(), ValidData.EN_DATE) +
                "', hireDate='" + cv.date2string(e.getHireDate(), ValidData.EN_DATE) +
                "', address='" + e.getAddress() + "', city='" + e.getCity() + "', country='" + e.getCountry() +
                "', homePhone='" + e.getHomePhone() + "', mobile='" + e.getMobile() + "', email='" + e.getEmail() +
                "', photoPath='" + e.getPhotoPath() + "', note='" + e.getNote() +
                "' WHERE employeeID=" + e.getEmployeeID();
        DAO dao = new DAO();
        System.out.println(sql);
        return dao.executeUpdateQuery(sql);
    }

    /**
     *
    Lay ve tat ca cac Employee co trong CSDL.
     */
    public Employee[] select() {
        String sql = "select * from hrmanager";
        DAO dao = new DAO();

        Vector vE = new Vector();
        try {
            ResultSet rs = dao.executeQuery(sql);
            while (rs.next()) {
                Employee e = new Employee();
                e.setEmployeeID(rs.getInt("EmployeeID"));
                e.setLastName(rs.getString("LastName"));
                e.setFirstName(rs.getString("FirstName"));
                e.setBirthDate(rs.getDate("BirthDate"));
                e.setHireDate(rs.getDate("HireDate"));
                e.setAddress(rs.getString("Address"));
                e.setCity(rs.getString("City"));
                e.setCountry(rs.getString("Country"));
                e.setHomePhone(rs.getString("HomePhone"));
                e.setMobile(rs.getString("Mobile"));
                e.setEmail(rs.getString("Email"));
                e.setPhotoPath(rs.getString("PhotoPath"));
                e.setNote(rs.getString("Note"));
                vE.add(e);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            dao.closeConnection();
            return null;
        }
        dao.closeConnection();
        return convertToArray(vE);
    }

    /**
     *
    Tim kiem Employee co trong CSDL.
     */
    public Employee[] find(int option, String value) {
        String sql = "";
        switch (option) {
            case 0:
                sql = "select * from hrmanager where firstname like '%" + value + "%' or lastname like '%" + value + "%'";
                break;
            case 1:
                sql = "select * from hrmanager where city like '%" + value + "%'";
                break;
        }
        DAO dao = new DAO();

        Vector vE = new Vector();
        try {
            ResultSet rs = dao.executeQuery(sql);
            while (rs.next()) {
                Employee e = new Employee();
                e.setEmployeeID(rs.getInt("EmployeeID"));
                e.setLastName(rs.getString("LastName"));
                e.setFirstName(rs.getString("FirstName"));
                e.setBirthDate(rs.getDate("BirthDate"));
                e.setHireDate(rs.getDate("HireDate"));
                e.setAddress(rs.getString("Address"));
                e.setCity(rs.getString("City"));
                e.setCountry(rs.getString("Country"));
                e.setHomePhone(rs.getString("HomePhone"));
                e.setMobile(rs.getString("Mobile"));
                e.setEmail(rs.getString("Email"));
                e.setPhotoPath(rs.getString("PhotoPath"));
                e.setNote(rs.getString("Note"));
                vE.add(e);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            dao.closeConnection();
            return null;
        }
        dao.closeConnection();
        return convertToArray(vE);
    }

    /**
     *
    get Employee by EmployeeID
     */
    public Employee getByID(int id) {
        String sql = "select * from hrmanager where EmployeeID=" + id;
        DAO dao = new DAO();
        try {
            ResultSet rs = dao.executeQuery(sql);
            if (rs.next()) {
                Employee e = new Employee();
                e.setEmployeeID(rs.getInt("EmployeeID"));
                e.setLastName(rs.getString("LastName"));
                e.setFirstName(rs.getString("FirstName"));
                e.setBirthDate(rs.getDate("BirthDate"));
                e.setHireDate(rs.getDate("HireDate"));
                e.setAddress(rs.getString("Address"));
                e.setCity(rs.getString("City"));
                e.setCountry(rs.getString("Country"));
                e.setHomePhone(rs.getString("HomePhone"));
                e.setMobile(rs.getString("Mobile"));
                e.setEmail(rs.getString("Email"));
                e.setPhotoPath(rs.getString("PhotoPath"));
                e.setNote(rs.getString("Note"));
                return e;
            }
        } catch (SQLException ex) {
            dao.closeConnection();
            return null;
        }
        dao.closeConnection();
        return null;
    }

    /**
     *
    Convert from Vector to Employee Array
     */
    private Employee[] convertToArray(Vector v) {
        int n = v.size();
        if (n < 1) {
            return null;
        }
        Employee[] arrEmployee = new Employee[n];
        for (int i = 0; i < n; i++) {
            arrEmployee[i] = (Employee) v.get(i);
        }
        return arrEmployee;
    }
}