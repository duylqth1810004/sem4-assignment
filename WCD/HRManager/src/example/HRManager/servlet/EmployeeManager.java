/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.HRManager.servlet;

import example.HRManager.bol.EmployeeBO;
import example.HRManager.entities.Employee;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class EmployeeManager extends HttpServlet {

//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        System.out.println("init() called first time!");
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
        System.out.println("service() called first time!");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
        }
        EmployeeBO ebo = new EmployeeBO();
        request.setAttribute("EmpBO", ebo);
        String value = request.getParameter("txtValue");
        String option = request.getParameter("ddlSearch");

        Employee[] emp = null;

        if (value == null || option == null) {
            emp = ebo.select();
            request.setAttribute("emp", emp);
            RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp");
            reqdis.forward(request, response);
        } else {
            if (option.equals("Name")) {
                emp = ebo.find(0, value);
            } else if (option.equals("City")) {
                emp = ebo.find(1, value);
            }

            request.setAttribute("emp" , emp);
            RequestDispatcher reqdis = request.getRequestDispatcher("employeeManager.jsp?option=" + option + "&value=" + value);
            reqdis.forward(request, response);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}