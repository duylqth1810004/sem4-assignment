/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package example.HRManager.servlet;

import example.ConvertData;
import example.HRManager.bol.EmployeeBO;
import example.HRManager.entities.Employee;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class ProcessEmployee extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("username") == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        EmployeeBO ebo = new EmployeeBO();
        Employee e;
        String action = request.getParameter("action");
        String id = request.getParameter("id");
        String fName = request.getParameter("txtFirstName");
        String lName = request.getParameter("txtLastName");
        String lB = "";
        if (lName == null || fName == null) {
            if (action == null || action.equals("add")) {
                e = new Employee();
            } else {
                e = ebo.getByID(Integer.parseInt(id));
                if(action.equals("delete")){
                    lB = "Delete";
                }else{
                    lB = "Update";
                }
            }
            request.setAttribute("action" , action);
            request.setAttribute("lB" , lB);
            request.setAttribute("e", e);
            RequestDispatcher reqdis = request.getRequestDispatcher("processEmployee.jsp");
            reqdis.forward(request, response);
            return;
        }

        String msg = "";
        if (action != null && action.equals("delete")) {
            try {
                e = ebo.getByID(Integer.parseInt(id));
                if (e == null) {
                    msg = "This Employee Id is not Exist!";
                } else {
                    if (ebo.delete(e) > 0) {
                        msg = "This Employee Id is Deleted!";
                    } else {
                        msg = "This Employee Id cannot be Deleted!";
                    }
                }
            } catch (NumberFormatException ex) {
                msg = "This isn't Employee Id";
                request.setAttribute("msg", msg);
                response.sendRedirect("InfomationPage.jsp?type=error&msg=This isn't Employee Id");
                return;
            }
        } else {
            e = new Employee();
            ConvertData convert = new ConvertData();
            e.setFirstName(fName);
            e.setLastName(lName);
            e.setBirthDate(convert.string2date(request.getParameter("txtBirthDate")));
            e.setHireDate(convert.string2date(request.getParameter("txtHireDate")));
            e.setAddress(request.getParameter("txtAddress"));
            e.setCity(request.getParameter("txtCity"));
            e.setCountry(request.getParameter("txtCountry"));
            e.setEmail(request.getParameter("txtEmail"));
            e.setHomePhone(request.getParameter("txtHomePhone"));
            e.setMobile(request.getParameter("txtMobile"));
            e.setNote(request.getParameter("txtNote"));

            if (action.equals("edit")) {
                try {
                    e.setEmployeeID(Integer.parseInt(id));
                    System.out.println(" ket qua"+ ebo.edit(e));
                    if (ebo.edit(e) > 0) {
                        msg = "This Employee Id is Updated!";
                    } else {
                        msg = "This EmployeeID cannot Update";
                    }
                } catch (NumberFormatException ex) {
                    msg = "This isn't Employee Id";
                    request.setAttribute("msg", msg);
                    response.sendRedirect("InformationPage.jsp");
                    return;
                }
            } else {
                System.out.println(" ket qua"+ ebo.edit(e));
                if (ebo.add(e) > 0) {
                    msg = "This EmployeeID inserted to database";
                } else {
                    msg = "This EmployeeID can't insert to database";
                }
            }
        }
        session.setAttribute("msg" , msg);
        response.sendRedirect("InformationPage.jsp");
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}