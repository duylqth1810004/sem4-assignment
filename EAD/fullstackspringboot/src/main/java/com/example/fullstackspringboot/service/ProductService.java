package com.example.fullstackspringboot.service;

import com.example.fullstackspringboot.entity.Product;
import com.example.fullstackspringboot.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository pR;

    public List<Product> findAll() {
        return pR.findAll();
    }

    public Optional<Product> findById(Long id) {
        return pR.findById(id);
    }

    public Product save(Product prod) {
        return pR.save(prod);
    }

    public void deleteById(Long id) {
        pR.deleteById(id);
    }
}
