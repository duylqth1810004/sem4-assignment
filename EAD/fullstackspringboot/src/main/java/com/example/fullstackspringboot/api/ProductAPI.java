package com.example.fullstackspringboot.api;

import com.example.fullstackspringboot.entity.Product;
import com.example.fullstackspringboot.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductAPI {
    private final ProductService pR;

    @GetMapping
    public ResponseEntity<List<Product>> findAll() {
        return ResponseEntity.ok(pR.findAll());
    }

    @PostMapping
    public ResponseEntity<Product> create(@Valid @RequestBody Product pro) {
        return ResponseEntity.ok(pR.save(pro));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable Long id) {
        Optional<Product> pro = pR.findById((id));

        if (!pro.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(pro.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable Long id, @Valid @RequestBody Product product) {
        Optional<Product> pro = pR.findById((id));

        if (!pro.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        product.setId(id);

        return ResponseEntity.ok(pR.save(product));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> delete(@PathVariable Long id) {
        Optional<Product> pro = pR.findById((id));

        if (!pro.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        pR.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
