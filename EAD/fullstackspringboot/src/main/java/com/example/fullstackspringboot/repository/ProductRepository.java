package com.example.fullstackspringboot.repository;

import com.example.fullstackspringboot.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
