package com.example.fsrestspbtodoapp.repository;

import com.example.fsrestspbtodoapp.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
