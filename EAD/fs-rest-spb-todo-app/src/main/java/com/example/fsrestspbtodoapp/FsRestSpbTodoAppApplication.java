package com.example.fsrestspbtodoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FsRestSpbTodoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(FsRestSpbTodoAppApplication.class, args);
    }

}
