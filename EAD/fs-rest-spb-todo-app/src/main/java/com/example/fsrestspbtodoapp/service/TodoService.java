package com.example.fsrestspbtodoapp.service;

import com.example.fsrestspbtodoapp.entity.Todo;
import com.example.fsrestspbtodoapp.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TodoService {
    private final TodoRepository tR;

    public List<Todo> findAll(){
        return tR.findAll();
    }

    public List<Todo> saveAll(List<Todo> todoList){
        return tR.saveAll(todoList);
    }
}
