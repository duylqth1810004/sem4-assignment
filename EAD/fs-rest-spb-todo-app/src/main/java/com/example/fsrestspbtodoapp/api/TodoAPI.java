package com.example.fsrestspbtodoapp.api;

import com.example.fsrestspbtodoapp.entity.Todo;
import com.example.fsrestspbtodoapp.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/todos")
public class TodoAPI {
    private final TodoService tS;

    @Autowired
    public TodoAPI(TodoService tS) {
        this.tS = tS;
    }

    @GetMapping
    public ResponseEntity<List<Todo>> findAll(){
        return ResponseEntity.ok(tS.findAll());
    }

    @PostMapping
    public ResponseEntity saveAll(@Valid @RequestBody List<Todo> todoList){
        return ResponseEntity.ok(tS.saveAll(todoList));
    }
}
